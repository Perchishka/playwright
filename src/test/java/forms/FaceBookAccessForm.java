package forms;

import com.microsoft.playwright.Page;
import pages.BasePage;
import pages.MiroBoardPage;

public class FaceBookAccessForm extends BasePage {

    public static final String CONTINUE_BUTTON = "[name='__CONFIRM__']";

    public FaceBookAccessForm(Page page) {
        super(page);
    }

    public MiroBoardPage confirmAccess() {
        page.click(CONTINUE_BUTTON);
        return new MiroBoardPage(page);
    }

    public String getFacebookAccName() {
        return page.innerText(CONTINUE_BUTTON);

    }
}
