package forms;

import com.microsoft.playwright.Page;
import enums.Providers;
import lombok.SneakyThrows;
import pages.*;

public class ReviewTermsForm extends BasePage {
    private final String TERMS_CHECKBOX = "[for='tos-signup-terms']";
    private final String SUBSCRIBE_CHECKBOX = "[for='tos-signup-subscribe']";
    private final String CONTINUE_BUTTON = "[data-autotest-id='mr-form-gdpr-btn-signin-1']";

    public ReviewTermsForm(Page page) {
        super(page);
    }

    @SneakyThrows
    public <T> T setCheckBoxAndContinue(Providers pr) {
        uiHelper.checkCheckBox(page, TERMS_CHECKBOX);
        uiHelper.checkCheckBox(page, SUBSCRIBE_CHECKBOX);
        page.click(CONTINUE_BUTTON);

        switch (pr) {
            case APPLE:
                return (T) new ApplePage(page);
            case GOOGLE:
                return (T) new GoogleAccPage(page);
            case SLACK:
                return (T) new SlackPage(page);
            case OFFICE365:
                return (T) new OfficePage(page);
            case FACEBOOK:
            default:
                return (T) new FaceBookPage(page);
        }
    }

}


