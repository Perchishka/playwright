package tests;

import com.microsoft.playwright.*;
import lombok.Data;
import org.junit.jupiter.api.*;
import pages.SignupPage;
import utils.PropertiesContext;
import utils.WebBrowser;

@Data
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BaseTestCase {
    // Shared between all tests in the class.
    BrowserContext context;
    Page page;
    SignupPage signupPage;
    WebBrowser browser = new WebBrowser();

    @BeforeEach
    void createContextAndPage() {
        browser.getBrowser();
        context = browser.getBrowser().newContext();
        page = context.newPage();
        openBasePage();
    }

    @AfterEach
    void closeContext() {
        browser.getBrowser().close();
        context.close();
    }

    public void openBasePage() {
        page.navigate(PropertiesContext.getInstance().getProperty("framework_baseurl"));
        signupPage = new SignupPage(getPage());
    }
}
