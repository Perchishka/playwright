package tests;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;

public class NegativeSignupTest extends BaseTestCase {

    @Test
    @DisplayName("Password length is at least 8 characters")
    public void signupWithShortPassword() {
        signupPage.fillField("password", "111");
        assertThat(signupPage.getPasswordMessage()).contains("Please use 8+ characters for secure password");
    }

    @Test
    @DisplayName("Password length < 61 symbols check")
    public void incorrectPasswordLength() {
        signupPage.fillField("name", RandomStringUtils.random(61, true, false));
        signupPage.fillField("email", "John..Doe@example.com");
        signupPage.fillField("password", RandomStringUtils.random(61, true, false));
        signupPage.setTermsCheckbox();
        signupPage.clickOnSubmitButton();
        assertThat(signupPage.getPasswordErrorMessage()).contains("Sorry, your password cannot exceed 60 characters");
    }

    @Test
    @DisplayName("Mandatory fields and checkboxes check")
    public void emptyMandatoryFields() {
        signupPage.clickOnSubmitButton();
        assertThat(signupPage.getNameErrorMessage()).contains("Please enter your name.");
        assertThat(signupPage.getEmailErrorMessage()).contains("Please enter your email address.");
        assertThat(signupPage.getEmptyPasswordMessage()).contains("Please enter your password.");
        assertThat(signupPage.getTermsError()).contains("Please agree with the Terms to sign up.");
    }

    @ParameterizedTest(name = "#{index} - invalid()? {0}")
    @DisplayName("Invalid email format check")
    @ValueSource(strings = {"%$&%", "111", "ру@ру", "john.smith(comment)@example.com"})
    public void incorrectEmailFormat(String email) {
        signupPage.fillField("email", email);
        signupPage.clickOnSubmitButton();
        assertThat(signupPage.getEmailErrorMessage()).contains("The email you entered is incorrect.");
    }

    @Test
    @DisplayName("Invalid email check")
    public void incorrectEmail() {
        signupPage.fillField("name", "Name");
        signupPage.fillField("email", "John..Doe@example.com");
        signupPage.fillField("password", "12345678");
        signupPage.setTermsCheckbox();
        signupPage.clickOnSubmitButton();
        assertThat(signupPage.getEmailErrorMessage())
                .contains("This doesn’t look like an email address. Please check it for typos and try again.");
    }
}
