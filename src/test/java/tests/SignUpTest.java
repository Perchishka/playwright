package tests;

import entities.users.UserFactory;
import enums.Providers;
import forms.FaceBookAccessForm;
import forms.ReviewTermsForm;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import pages.*;

import static org.assertj.core.api.Assertions.assertThat;


public class SignUpTest extends BaseTestCase {

    @Test
    @DisplayName("Signup with mandatory fields and checkboxes only")
    public void signupEmailMandatoryFieldsTest() {
        var email = UserFactory.createUser().getEmail();
        signupPage.fillUserData(UserFactory.createUser().getName(), email,
                UserFactory.createUser().getPassword());
        signupPage.setTermsCheckbox();
        CheckYourEmailPage checkYourEmailPage = signupPage.clickOnSubmitButton();
        assertThat(checkYourEmailPage.getHeaderText()).contains("Check your email");
        assertThat(checkYourEmailPage.getEmail()).contains(email);
    }

    @ParameterizedTest(name = "#{index} - Signup with name {0} and password {1} - {2}")
    @DisplayName("Signup using special characters, emoji, numbers, other language")
    @CsvSource({
            "\uD83D\uDE30 ,12345678, Weak password",
            "890.00, 1234_56789Aa, Good password",
            "руууу, _56789Aa$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$_23cc, Great password",
            "Inna%^&*, Abcdef1234!, So-so password"
    })

    public void signupWithEmailTest(String name, String password, String message) {
        var email = UserFactory.createUser().getEmail();
        signupPage.fillField("name", name);
        signupPage.fillField("email", email);
        signupPage.fillField("password", password);
        assertThat(signupPage.getPasswordMessage()).contains(message);
        signupPage.setTermsCheckbox();
        signupPage.setUpdatesCheckbox();
        CheckYourEmailPage checkYourEmailPage = signupPage.clickOnSubmitButton();
        assertThat(checkYourEmailPage.getHeaderText()).contains("Check your email");
        assertThat(checkYourEmailPage.getEmail()).contains(email);
    }

    @Test
    @DisplayName("Signup with Facebook")
    public void signUpWithFacebookTest() {
        ReviewTermsForm reviewTermsForm = signupPage.clickOnProviderButton(Providers.FACEBOOK.getDesc());
        FaceBookPage faceBookPage = reviewTermsForm.setCheckBoxAndContinue(Providers.FACEBOOK);
        assertThat(faceBookPage.getTitle()).contains("Facebook");
        FaceBookAccessForm faceBookAccessForm = faceBookPage.login("tt4758709@gmail.com", "tt4758709_123");
        assertThat(faceBookAccessForm.getFacebookAccName()).contains("TestAccount");
        MiroBoardPage miroSetUpPage = faceBookAccessForm.confirmAccess();
        assertThat(miroSetUpPage.checkHeaderVisibility()).isTrue();
    }

    @Test
    @DisplayName("Signup with Slack")
    public void signUpWithSlackTest() {
        ReviewTermsForm reviewTermsForm = signupPage.clickOnProviderButton(Providers.SLACK.getDesc());
        SlackPage slackPage = reviewTermsForm.setCheckBoxAndContinue(Providers.SLACK);
        assertThat(slackPage.getTitle()).contains("Slack");
    }

    @Test
    @DisplayName("Signup with Office365")
    public void signUpWithOffice365() {
        ReviewTermsForm reviewTermsForm = signupPage.clickOnProviderButton(Providers.OFFICE365.getDesc());
        OfficePage officePage = reviewTermsForm.setCheckBoxAndContinue(Providers.OFFICE365);
        assertThat(officePage.checkVisibility()).isTrue();
    }

    @Test
    @DisplayName("Signup with Google")
    public void signUpWithGoogle() {
        ReviewTermsForm reviewTermsForm = signupPage.clickOnGoogleBtn();
        GoogleAccPage googleAccPage = reviewTermsForm.setCheckBoxAndContinue(Providers.GOOGLE);
        assertThat(googleAccPage.getTitle()).contains("Google");
    }

    @Test
    @DisplayName("Signup with AppleId")
    public void signUpWithApple() {
        ReviewTermsForm reviewTermsForm = signupPage.clickOnProviderButton(Providers.APPLE.getDesc());
        ApplePage applePage = reviewTermsForm.setCheckBoxAndContinue(Providers.APPLE);
        assertThat(applePage.getTitle()).contains("Apple");
    }
}
