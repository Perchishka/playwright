package entities.users;

import com.github.javafaker.Faker;

public class UserFactory {

    private static final Faker FAKER = new Faker();

    public static User createUser() {
        return new User(FAKER.name().fullName(), FAKER.internet().emailAddress().replace("@", "_INVALID@"),
                FAKER.internet().password(8, 12));
    }
}
