package utils;

import com.microsoft.playwright.Browser;
import com.microsoft.playwright.BrowserType;
import com.microsoft.playwright.Playwright;

public class WebBrowser {

    Playwright playwright;
    Browser browser;

    /**
     * Check browser type in properties file and set it.
     * @return created instance of BrowserType
     */
    public BrowserType getBrowserType() {
        String browserName = PropertiesContext.getInstance().getProperty("browser");
        switch (browserName) {
            case "chrome":
                return playwright.chromium();
            case "webkit":
                return playwright.webkit();
            case "firefox":
                return playwright.firefox();
            default:
                throw new IllegalArgumentException();
        }
    }
    /**
     * Check OS type in properties file and set launch options.
     * @return created instance of Browser
     */
    public Browser getBrowser() {
        playwright = Playwright.create();
        switch (OperationSystemInfo.getOs()) {
            case UNIX:
                browser = getBrowserType().launch();
                break;
            case WINDOWS:
            default:
                browser = getBrowserType()
                        .launch(new BrowserType.LaunchOptions()
                                .setHeadless(false).setSlowMo(500));
                break;
        }
        return browser;
    }

}