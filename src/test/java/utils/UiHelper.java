package utils;

import com.microsoft.playwright.Page;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UiHelper {
    /**
     * Set checkbox
     */
    public void checkCheckBox(Page page, String selector){
        if (!page.isChecked(selector)) {
            page.check(selector);
        } else {
            log.info("Checkbox is already checked");
        }
    }

}
