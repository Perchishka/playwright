package utils;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Properties;

/**
 * Properties Context Class. Used in whole project to get config of all used systems from *.properties files in /resources folder. <br>
 * Loads list of properties from resources folder to be available in system in singleton manner. <br>
 */
@Slf4j
public class PropertiesContext {

    /**
     * Name of Properties file for whole environment.
     */
    private static final String ENV_PROPERTIES = "env";

    /**
     * Name of Properties file for uiMap.
     */
    private static final String UI_PROPERTIES = "uiMap";
    /**
     * Static variable for singleton instance of PropertiesContext class
     */
    private static PropertiesContext instance = new PropertiesContext();
    /**
     * Used for loading properties from file with whole environment properties and saving it into the General Map
     */
    private Properties envMap = new Properties();

    /**
     * Used for loading properties from file with uiMap properties and saving it into the General Map
     */
    private Properties uiMap = new Properties();
    /**
     * Used for loading properties from file with userMap properties and saving it into the General Map
     */
    private Properties userMap = new Properties();

    /**
     * General Map for keeping all properties in this variable.
     */
    private Properties generalMap = new Properties();

    /**
     * Initialize PropertyContext. Call method init()
     *
     * @see #init()
     */
    private PropertiesContext() {
        init();
    }

    /**
     * Create a new PropertiesContext instance if it's not created before and store in private static variable
     *
     * @return created instance of PropertiesContext singleton
     */
    public static PropertiesContext getInstance() {
        if (instance == null) {
            instance = new PropertiesContext();
        }
        return instance;
    }

    /**
     * Load properties from *.properties files in /resources folder. Save all properties into one private variable generalMap
     *
     * @see #generalMap
     */
    private void init() {
        loadPropertiesFromClasspath(envMap, ENV_PROPERTIES);
        loadPropertiesFromClasspath(uiMap, UI_PROPERTIES);
        generalMap.putAll(envMap);
        generalMap.putAll(uiMap);
        generalMap.putAll(userMap);

        if (System.getProperty("envurl") != null) {
            generalMap.setProperty("envurl", System.getProperty("envurl"));
        }
        if (System.getProperty("browser") != null) {
            generalMap.setProperty("browser", System.getProperty("browser"));
        }
        if (System.getProperty("maxRetryCount") != null) {
            generalMap.setProperty("maxRetryCount", System.getProperty("maxRetryCount"));
        }
        if (System.getProperty("threadcount") != null) {
            generalMap.setProperty("threadcount", System.getProperty("threadcount"));
        }

        if (System.getProperty("networkFileDir") != null) {
            generalMap.setProperty("networkFileDir", System.getProperty("networkFileDir"));
        }
    }

    /**
     * Return loaded into field generalMap property by specified key.
     *
     * @param key The name of required property
     * @return Value of loaded property
     * @throws NullPointerException If key wasn't found in the generalMap field
     */
    public String getProperty(String key) {
        String result = (String) generalMap.get(key);
        if (result != null) {
            return result;
        } else {
            throw new NullPointerException("Property " + key + " was not found");
        }
    }

    /**
     * Set property specified by key into specified value in generalMap field.
     *
     * @param key   The name of property
     * @param value The value that must be set
     */
    public void setProperty(String key, String value) {
        generalMap.setProperty(key, value);
    }

    /**
     * Clear generalMap field from all values.
     */
    public void clear() {
        generalMap.clear();
    }

    /**
     * Return full name of file with properties.
     *
     * @param fileName Name of file without extension
     * @return full name of file
     */
    private String getFullFileName(String fileName) {
        return fileName + ".properties";
    }


    /**
     * Load properties of specified group from file _group_.properties in /resources folder.
     *
     * @param props    Variable where loaded properties stored
     * @param fileName Name of file that must be loaded
     */
    private void loadPropertiesFromClasspath(Properties props, String fileName) {
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            InputStream resourceAsStream = classLoader.getResourceAsStream(getFullFileName(fileName));


            if (resourceAsStream != null) {
                props.load(resourceAsStream);
            }
        } catch (IOException e) {
            log.info("Missing or corrupt property file");
        }
    }
}
