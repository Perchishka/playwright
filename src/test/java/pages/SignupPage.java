package pages;

import com.microsoft.playwright.Page;
import forms.ReviewTermsForm;

public class SignupPage extends BasePage {

    private final String NAME_FIELD = ".signup #name";
    private final String EMAIL_FIELD = ".signup #email";
    private final String PASSWORD_FIELD = ".signup #password";
    private final String TERMS_CHECKBOX = "label[for='signup-terms']";
    private final String UPDATE_CHECKBOX = "label[for='signup-subscribe']";
    private final String SUBMIT_BUTTON = "[data-autotest-id='mr-form-signup-btn-start-1']";
    private final String SIGNUP_GOOGLE = "#kmq-google-button";
    private final String NAME_ERROR = ".signup #name ~ div #nameError";
    private final String EMAIL_ERROR = ".signup #email ~ div #emailError";
    private final String PASSWORD_ERROR = ".signup #password ~ div #passwordError";
    private final String EMPTY_PASSWORD_ERROR = "[data-autotest-id='please-enter-your-password-1']";
    private final String PASSWORD_MESSAGE = ".signup__input-hint-text";
    private final String TERMS_ERROR = "#termsError";
    private final String PROVIDERS_BUTTONS = ".signup__social-container img[alt='%s']";
    private final String SIGNUP_FIELDS = ".signup__input-wrap-ico ~ [id='%s']";

    public SignupPage(Page page) {
        super(page);
    }

    public ReviewTermsForm clickOnProviderButton(String provider) {
        page.click(String.format(PROVIDERS_BUTTONS, provider));
        return new ReviewTermsForm(page);
    }

    public ReviewTermsForm clickOnGoogleBtn() {
        page.click(SIGNUP_GOOGLE);
        return new ReviewTermsForm(page);
    }

    public void fillUserData(String name, String email, String password) {
        page.fill(NAME_FIELD, name);
        page.fill(EMAIL_FIELD, email);
        page.fill(PASSWORD_FIELD, password);
    }

    public void fillField(String name, String data) {
        page.fill(String.format(SIGNUP_FIELDS, name), data);
    }

    public void setTermsCheckbox() {
        uiHelper.checkCheckBox(page, TERMS_CHECKBOX);
    }

    public void setUpdatesCheckbox() {
        uiHelper.checkCheckBox(page, UPDATE_CHECKBOX);
    }

    public CheckYourEmailPage clickOnSubmitButton() {
        page.click(SUBMIT_BUTTON);
        return new CheckYourEmailPage(page);
    }

    public String getPasswordMessage() {
        page.waitForLoadState();
        return page.innerText(PASSWORD_MESSAGE);
    }

    public String getEmailErrorMessage() {
        return page.innerText(EMAIL_ERROR);
    }

    public String getNameErrorMessage() {
        return page.innerText(NAME_ERROR);
    }

    public String getEmptyPasswordMessage() {
        return page.innerText(EMPTY_PASSWORD_ERROR);
    }

    public String getTermsError() {
        return page.innerText(TERMS_ERROR);
    }

    public String getPasswordErrorMessage() {
        return page.innerText(PASSWORD_ERROR);
    }
}