package pages;

import com.microsoft.playwright.Page;

public class OfficePage extends BasePage {
    public static String SIGNIN_OPTIONS = "[data-test-id='signinOptions']";

    public OfficePage(Page page) {
        super(page);
    }

    public boolean checkVisibility() {
        page.waitForSelector(SIGNIN_OPTIONS);
        return page.isVisible(SIGNIN_OPTIONS);
    }
}
