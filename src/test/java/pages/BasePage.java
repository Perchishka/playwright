package pages;

import com.microsoft.playwright.Page;
import utils.UiHelper;

public class BasePage {

    public final Page page;
    protected UiHelper uiHelper = new UiHelper();

    public BasePage(Page page) {
        this.page = page;
    }

    public String getTitle() {
        return page.title();
    }
}
