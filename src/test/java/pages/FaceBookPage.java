package pages;

import com.microsoft.playwright.Page;
import forms.FaceBookAccessForm;

public class FaceBookPage extends BasePage {

    public static final String ACCEPT_BUTTON = "[data-cookiebanner='accept_button']";
    public static final String EMAIL = "#email_container input";
    public static final String PASSWORD = "#pass";
    public static final String LOGIN_BUTTON = "#loginbutton";

    public FaceBookPage(Page page) {
        super(page);
    }

    public String getTitle() {
        page.click(ACCEPT_BUTTON);
        return page.title();
    }

    public FaceBookAccessForm login(String email, String password) {
        page.fill(EMAIL, email);
        page.fill(PASSWORD, password);
        page.click(LOGIN_BUTTON);
        return new FaceBookAccessForm(page);
    }
}
