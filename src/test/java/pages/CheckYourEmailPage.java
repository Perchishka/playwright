package pages;

import com.microsoft.playwright.Page;

public class CheckYourEmailPage extends BasePage {

    public static final String CHECK_YOUR_EMAIL = ".signup__title-form";
    public static final String CONFIRM_EMAIL = ".signup__subtitle-form strong";

    public CheckYourEmailPage(Page page) {
        super(page);
    }

    public String getHeaderText() {
        return page.innerText(CHECK_YOUR_EMAIL);
    }

    public String getEmail() {
        return page.innerText(CONFIRM_EMAIL);
    }

}