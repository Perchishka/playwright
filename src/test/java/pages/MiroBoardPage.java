package pages;

import com.microsoft.playwright.Page;

public class MiroBoardPage extends BasePage {
    public static final String HEADER_SELECTOR = "[aria-label='Miro logo']";

    public MiroBoardPage(Page page) {
        super(page);
    }

    public boolean checkHeaderVisibility() {
        page.waitForSelector(HEADER_SELECTOR);
        return page.isVisible(HEADER_SELECTOR);

    }
}
