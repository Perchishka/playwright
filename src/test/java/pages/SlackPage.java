package pages;

import com.microsoft.playwright.Page;

public class SlackPage extends BasePage {

    public static String ACCEPT_BUTTON = "#onetrust-button-group #onetrust-accept-btn-handler";

    public SlackPage(Page page) {
        super(page);
    }

    public String getTitle() {
        page.click(ACCEPT_BUTTON);
        return page.title();
    }
}
