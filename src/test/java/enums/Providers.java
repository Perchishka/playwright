package enums;

public enum Providers {

    FACEBOOK("Sign up with Facebook"),
    APPLE("Sign up with Apple"),
    GOOGLE("Sign up with Google"),
    OFFICE365("Sign up with Office 365"),
    SLACK("Sign up with Slack");

    private final String desc;

    Providers(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
