# MIRO SIGN UP PAGE TESTING

## Authors and acknowledgment

- [ ] Ekaterina Pertseva

## Project status

- [ ] In progress

## Project structure

### Tools and Technologies:

Java, Maven, Junit5, Playwright, Faker, AssertJ

### Project contains packages:

- entities.users
- forms
- pages
- tests
- utils

## Browser configurations

This project contains basic browser configurations. You can find them in the package: utils -> WebBrowser In the future,
the configuration settings will be expanded depending on the needs of the project

```
public Browser getBrowser() {
        playwright = Playwright.create();
        switch (OperationSystemInfo.getOs()) {
            case UNIX:
                browser = getBrowserType().launch();
                break;
            case WINDOWS:
            default:
                browser = getBrowserType()
                        .launch(new BrowserType.LaunchOptions()
                                .setHeadless(false).setSlowMo(600));
                break;
        }
        return browser;
    }
```

## Tests

This framework contains end-to-end and functional test cases. All test cases are located in "tests" package. Test
framework can run test cases in the headless mode or not depends on OS. For OS=Windows there is a slowMo option for
visibility. It can be removed.

### TODO list:

- [ ]  Add functionality to remove access in Facebook test account settings. At this moment it can be performed
  manually.
- [ ]  Test cases for signup with Google, Slack, Office365, Apple require test accounts to complete end-to-end tests.
- [ ]  Add accept cookies method
